<?php

	/***********
	* FUNCTION: getDateSelect
	* PURPOSE: Get the selected date option from a form field
	* CREDIT: Alex Woehr (@alex_woehr)
	*/

function getDateSelect($selected = "today") {

	$date_options = Array(
		"today" => "Today",
		"tomorrow" => "Tomorrow",
		"upcoming" => "Upcoming",
		"someday" => "Someday",
	);

	// Default to "today" if they send an invalid input day
	if (!array_key_exists($selected, $date_options)) {
		// could raise an error if we cared
		// choose first key
		$dates = array_keys($date_options);
		$selected = $dates[0];
	}

	// easier to use single quotes for HTML strings
	$output = "<select name='date'>";
	foreach ($date_options as $date_name => $date_display) {
		// Select this option if necessary
		$selected_str = ""; // empty by default
		if ($selected == $date_name) $selected_str = " selected ";
		// Append option to output
		$output .= "<option value='$date_name' $selected_str>$date_display</option>";
	}
	$output .= "</select>";
	return $output;
}

	/***********
	* FUNCTION: getImportanceSelect
	* PURPOSE: Get the importance a user has selected from the "Importance" drop down
	* CREDIT: Alex Woehr (@alex_woehr)
	*/

function getImportanceSelect($selected = "normal") {
	$options = Array(
		"high" => "High",
		"normal" => "Normal",
		"low" => "Low",
	);

	// Default to "today" if they send an invalid input day
	if (!array_key_exists($selected, $options)) {
		// could raise an error if we cared
		// default to first key
		$option_names = array_keys($options);
		$selected = $option_names[0];
	}

	// easier to use single quotes for HTML strings
	$output = "<select name='importance'>";
	foreach ($options as $option_name => $option_display) {
		// Select this option if necessary
		$selected_str = ""; // empty by default
		if ($selected == $option_name) $selected_str = " selected ";
		// Append option to output
		$output .= "<option value='$option_name' $selected_str>$option_display</option>";
	}
	// Cap it off
	$output .= "</select>";
	return $output;
}

?>
