# WhenDos95

_The to-do app for your 9-5 workday!_

![ScreenShot](images/whendos95.png)

## INTRO
This is a simple to-do app created for personal learning. The code is freely available under the GPL v3 license. I will be adding to the project as I have time.

## TECHNOLOGIES USED
* PHP 5.5.3

## VERSIONS

NOTE: I use [Semantic Versioning](http://semver.org/) for this app.

### 0.4.0

* Added a Win95 UI true to the app's name - enjoy!
* Turns out session variables aren't truly RESTful, so let me add that caveat to the jubilance of my last commit

### 0.3.0
* Turns out my initial RESTful rewrite wasn't 100% RESTful, so this update should address some shortcomings, namely:
	* Forms submit to .php files vs. passing a mode parameter; thus, adding a to-do actually sends you to add.php vs. calling a method in add.php
	* I use HTTP headers to handle flow between files vs. relying on method calls to move between files

* The app now uses PHP sessions to persist data between files (it was lots of fun(?) getting this to work, but I learned a lot!)

### 0.2.0
* RESTful rewrite - One resource per URL
* General clean up and better code documentation through comments
* Kudos to [@alex_woehr](https://twitter.com/alex_woehr) for help making the edit mode code-driven (vs. the editor driven approach I took in the first version of this app)

### 0.1.0
* Crafted basic program logic
* Added the ability to read in a text file of CSV-formatted to-do items and display them in a browser
* Created add, edit, and delete to-do functionality

## Things Learned
* Use the `htmlentities()` function when you output generic HTML; use `htmlspecialchars()` if you're inside an attribute.
* For Javascript, use the following pattern to output HTML: `var myData = decodeURIComponent("<?php echo rawurlencode($myData); ? >");`


