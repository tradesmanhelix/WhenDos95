<?php

        /***********
         *  APP: WhenDos95 - Basic to-do app
         *  Coder: Alex Smith
         *  Language: PHP 5.5.3 
        */


        /* 
        *   PURPOSE: Starts the session for sharing data between scripts
	*   Not very useful, but broke it out into a separate files in case I need to do anything fancier later on
        */

session_start();

?>
