<?PHP

        /***********
         *  APP: Whendos95 - Basic to-do app
         *  CODER: Alex Smith
         *  LANGUAGE: PHP 5.5.3
         */

        /***********
        *   FUNCTION: deleteToDos
        *   PURPOSE: Removes a to-do item from the CSV database
        */

require_once("./session.php");
require_once("./config.php");

$handle = fopen(TODO_FILE, 'r') or die('delete.php: Cannot open file:  ' . TODO_FILE);

$todo_array = array();

if($_SESSION['to-do'] == '0') {
	$delete_item = $_POST['id'];
} else {
	$delete_item = $_SESSION['to-do'];
}

$counter = 1;
		
// Get the list of to-dos as an array of CSVs and add those arrays to an array
while (($data = fgetcsv($handle, 1000, ",")) != FALSE) {
		
	// Only add the to-do if it's not the one to delete as specified by $_POST['id']
	if ($counter != $delete_item) {
		array_push($todo_array, $data);
	}	
	$counter++;
}
		
fclose($handle);

// Loop through our array of CSV to-dos and add them to our file
$handle = fopen(TODO_FILE, 'w') or die('delete.php: Cannot open file:  ' . TODO_FILE);

for ($c = 0; $c < count($todo_array); $c++) {
	fputcsv($handle, $todo_array[$c]);
}

$_SESSION['to-do'] = '0';
header("HTTP/1.1 303 See Other");
header('Location: ./index.php');


?>
