<?PHP
	/***********
	 *  APP: Whendos95 - A basic to-do app
	 *  CODER: Alex Smith
	 *  LANGUAGE: PHP 5.5.3
	 */
?>

<!-- Include the necessary files -->
<?php

	// Helper files
	require_once("./session.php");
	require_once("./config.php");

	// Set the necessary session variables
	$_SESSION['to-do'] = '0';
?>

<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" type="text/css" href="tables.css">
<link rel="stylesheet" type="text/css" href="styles95.css">
<title>WhenDos</title>
</head>
<div id="wrapper">
<?php

	/***********
	*   FUNCTION: processToDos
	*   PURPOSE: Handles the main program flow
	*/

function processToDos() {
	
	if (($handle = fopen(TODO_FILE, "r")) === FALSE) return;

	?>
	<div id="dialogue"></div>
	<h1>
	<div id="dialogue_header">
	<div id="exit_button"></div>
	</div>
	<span id="welcome"><span id="welcome-font">Welcome to</span> <span id="whendos">WhenDos</span></span><span id="ninety-five">95</span>
	</h1>
	<div class="table p20">
	<div>
	<div class="header">Task</div>
	<div class="header">When</div>
	<div class="header">Importance</div>
	</div>

	<?php	

	$todo_id = 1; // variable for assigning an ID to each to-do
	while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
		echo '<div id="tr">';
		for ($c = 0; $c < count($data); $c++) {
			echo '<div id="td">' . htmlentities($data[$c]) . '</div>';
		}
		
		echo '<form action="./edit.php" method="POST">';
		echo '<div id="edit">';
		echo '<input type="hidden" name="id" value="' . $todo_id  . '" >';
		echo '<input type="submit" value="" id="edit">';
		echo '</div>';
		echo '</form>';
		
		
		echo '<form action="./delete.php" method="POST">';
		echo '<div id="delete">';
		echo '<input type="hidden" name="id" value="' . $todo_id  . '" >';
		echo '<input type="submit" value="" id="delete">';
		echo '</div>';
		echo '</form>';

		echo '</div>';
		$todo_id++;
	}
	?>
	</div>
	<hr id="divider">
	<?php
	fclose($handle);
}		 

?>
<body>
<main>
<?php
	processToDos();
?>
</main>
<br />
<form action="./add.php" method="POST">
To Do: <input type="text" name="to-do"><br />
When: 
<select name="date">
<option value="today" selected>Today</option>
<option value="tomorrow">Tomorrow</option>
<option value="upcoming">Upcoming</option>
<option value="someday">Someday</option>
</select><br />
Importance:
<select name="importance">
<option value="high">High</option>
<option value="normal" selected>Normal</option>
<option value="low">Low</option>
</select><br /><br />
<input type="submit" id="startimage-button" value="">
</form>
</div>
</body>
</html>
