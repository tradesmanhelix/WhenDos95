<?PHP
	/***********
	 *  APP: WhenDos95 - Basic to-do app
	 *  Coder: Alex Smith
	 *  Language: PHP 5.5.3 
 	*/


	/* 
	*   PURPOSE: Opens the to-do file to append a new to-do
	*/

require_once("./session.php");
require_once("./config.php");

$handle = fopen(TODO_FILE, 'a') or die('add.php: Cannot open file:  ' . TODO_FILE);

fputcsv($handle, $_POST);

fclose($handle);

if($_SESSION['to-do'] == '0') {
	// Set the HTTP code and return the user to the index page
	header("HTTP/1.1 303 See Other");
	header('Location: ./index.php');
} else {
	// Set the HTTP code and jump to delete
	// This request came from edit.php; we've just added the updated to-do to the list
	// Now, we need to remove the old to-do from the database
	header("HTTP/1.1 303 See Other");
	header('Location: ./delete.php');
}

?>
