<?PHP

	/***********   
	 *   APP: Whendos95 - Basic to-do app
         *   CODER: Alex Smith
         *   LANGUAGE: PHP 5.5.3
         */

	/***********
	*   FUNCTION: editToDo
	*   PURPOSE: Edits an existing to-do
	*/

require_once("./session.php");
require_once("./functions.php");
require_once("./config.php");

$handle = fopen(TODO_FILE, 'r') or die('edit.php: Cannot open file:  ' . TODO_FILE);

$todo_array = array();
$edit_item = $_POST['id'];
$data = array();
		
// Get the list of to-dos as an array of CSVs and add those arrays to an array
// Since the ID of a to-do is always >= 1, we need to start at 1 to avoid an extra trip through this loop
for ($c = 1; $c <= $edit_item; $c++) {
	$data = fgetcsv($handle, 1000, ",");
	if ($c == $edit_item) {
		$d = 0;
		echo '<!DOCTYPE html><html><head><link rel="stylesheet" type="text/css" href="styles95.css"><title>WhenDos</title></head><body>';
		echo '<form action="./add.php" method="POST">';
		echo '<input type="text" name="to-do" value="' . $data[$d] . '" > &nbsp;';
				
		// Search for the correct date value and output it as selected in the HTML selection box
		echo getDateSelect($data[++$d])."&nbsp;";

		// Search for the correct priority and output it as selected in the HTML select box
		echo getImportanceSelect($data[++$d])."&nbsp;";
				
		// Set the mode so we'll add the new version of the to-do when the user submits the above form
		echo '<input type="submit" value="Update">';
		echo '</form>';

		echo '<form method="link" action="index.php"><input type="submit" value="Cancel" style="margin-left: 1em;"></form>';
		echo '</body></html>';

       		fclose($handle); // close the file so other methods can access it
	
		$c = $edit_item + 1; // change this so we'll drop out of the for loop
	}
}

$_SESSION['to-do'] = $edit_item;

?>
